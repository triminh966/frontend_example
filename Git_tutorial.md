1. Using "git status" to view all new file, modified files, removed files. If the file is in red color, it means that the file is not added to commit zone(will not be pushed to server resource.).
Green color text means the file is in commit zone, if we using "git push", all green files will be commited to server resources.
2. Using "git add" to register wanted files to commit zone.
3. Using "git commit -m 'your message'" to push all file to commit zone.
4. Using "git push" to push all files in commit zone to server resources.


Note: "git reset" to reset all changes. "git pull" to update resource from server to local. 