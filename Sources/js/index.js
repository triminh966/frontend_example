function add(a, b) {
	return a + b;
}

function substract(a, b) {
	return a - b;
}

function multiply(a, b) {
	return a * b;
}

function devide(a, b) {
	return a / b;
}

function squareroot(a, b) {
	return Math.pow(a, 1/b);
}

function square(a, b) {
	return Math.pow(a, b);
}

function isValid(value) {
	if(value != null && value != undefined && value.length > 0) {
		return true;
	} else {
		return false;
	}
}

function calSquare() {
  var ip1 = document.getElementById("input1").value;
  var ip2 = document.getElementById("input2").value;
  
  if(isValid(ip1) && isValid(ip2)) {
	var n1 = Number(ip1);
	var n2 = Number(ip2);
	
    var rs = square(n1, n2);
    
	// Set data to result input control
	document.getElementById("result").value = rs;
  } else {
	console.log("Your input data is invalid!!");
  }
}

function calSquareroot() {
  var ip1 = document.getElementById("input1").value;
  var ip2 = document.getElementById("input2").value;
  
  if(isValid(ip1) && isValid(ip2)) {
	var n1 = Number(ip1);
	var n2 = Number(ip2);
	
    var rs = squareroot(n1, n2);
    
	// Set data to result input control
	document.getElementById("result").value = rs;
  } else {
	console.log("Your input data is invalid!!");
  }
}

function calDevide() {
  var ip1 = document.getElementById("input1").value;
  var ip2 = document.getElementById("input2").value;
  
  if(isValid(ip1) && isValid(ip2)) {
	var n1 = Number(ip1);
	var n2 = Number(ip2);
	
    var rs = devide(n1, n2);
    
	// Set data to result input control
	document.getElementById("result").value = rs;
  } else {
	console.log("Your input data is invalid!!");
  }
}


function calMultiply() {
  var ip1 = document.getElementById("input1").value;
  var ip2 = document.getElementById("input2").value;
  
  if(isValid(ip1) && isValid(ip2)) {
	var n1 = Number(ip1);
	var n2 = Number(ip2);
	
    var rs = multiply(n1, n2);
    
	// Set data to result input control
	document.getElementById("result").value = rs;
  } else {
	console.log("Your input data is invalid!!");
  }
}

function calSubtract() {
  var ip1 = document.getElementById("input1").value;
  var ip2 = document.getElementById("input2").value;

  if(isValid(ip1) && isValid(ip2)) {
	var n1 = Number(ip1);
	var n2 = Number(ip2);
	
	var rs = substract(n1, n2);
	
    // Set data to result input control
	document.getElementById("result").value = rs;
  } else {
	console.log("Your input data is invalid!!");
  }  

}

function calAdd() {
  var ip1 = document.getElementById("input1").value;
  var ip2 = document.getElementById("input2").value;
  
  if(isValid(ip1) && isValid(ip2)) {
	var n1 = Number(ip1);
	var n2 = Number(ip2);
	
    var rs = add(n1, n2);
    
	// Set data to result input control
	document.getElementById("result").value = rs;
  } else {
	console.log("Your input data is invalid!!");
  }
}

function clearAll() {
	document.getElementById("input1").value = "";
	document.getElementById("input2").value = "";
	document.getElementById("result").value = "";
}